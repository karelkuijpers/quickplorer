<?php
defined ("TYPO3") or die("Access denied");
//$path=TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('quickplorer');
TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addModule(
	"tools",
	"quickplorer",
	"",
	"",
	array(
		'routeTarget' => Parousia\Quickplorer\Controller\quickplorer::class.'::mainAction',
		'access' => 'admin',
		'name' => 'tools_quickplorer',
		'icon' => 'EXT:quickplorer/Resources/Public/Icons/moduleicon.gif',
		'labels' => 'LLL:EXT:quickplorer/Resources/Private/Language/locallang_mod.xml'
	)
);
