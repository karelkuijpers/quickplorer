<?php
namespace Parousia\Quickplorer\Controller;
/***************************************************************
*  Copyright notice
*  
*  (c) 2004 Mads Brunn (madsbrunn@gmail.com)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is 
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
* 
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
* 
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/** 
 * class 'quickplorer_search' for the 'quickplorer' extension.
 * contains functions to display searchform and searchresults and
 * to search for files and folders 
 *
 * @author	Karel Kuijpers <karelkuijpers@gmail.com>
 */
/***************************************************************

     The Original Code is fun_search.php, released on 2003-03-31.

     The Initial Developer of the Original Code is The QuiX project.
	 
	 quix@free.fr
	 http://www.quix.tk
	 http://quickplorer.sourceforge.net

****************************************************************/
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Backend\Utility\IconUtility;
use Parousia\Quickplorer\Hooks\quickplorer_div;


class quickplorer_search{

	var $contents=array();

	function find_item($dir,$pat,&$list,$recur) {	// find items
		$handle=@opendir(quickplorer_div::get_abs_dir($dir));
		if($handle===false) return;		// unable to open dir
		
		while(($new_item=readdir($handle))!==false) {
			if(!@file_exists(quickplorer_div::get_abs_item($dir, $new_item))) continue;
			if(!quickplorer_div::get_show_item($dir, $new_item)) continue;
			
			// match?
			if(@preg_match('/'.$pat.'/',$new_item)) $list[]=array($dir,$new_item);
			
			// search sub-directories
			if(quickplorer_div::get_is_dir($dir, $new_item) && $recur) {
				$this->find_item(quickplorer_div::get_rel_item($dir,$new_item),$pat,$list,$recur);
			}
		}
		
		closedir($handle);
	}
	//------------------------------------------------------------------------------
	function make_list($dir,$item,$subdir) {	// make list of found items
		// convert shell-wildcards to PCRE Regex Syntax
		$pat='^'.str_replace('?','.',str_replace('*','.*',str_replace('.',"\.",$item)))."$";
		
		// search
		$this->find_item($dir,$pat,$list,$subdir);
		if(is_array($list)) sort($list);
		return $list;
	}
	//------------------------------------------------------------------------------
	function print_table($list) {			// print table of found items
		global $BACK_PATH;

		if(!is_array($list)) return;
		
		$imagepath = '../'.PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('quickplorer')).'Classes/Controller/_img/';
		$imagepath = '/typo3conf/ext/quickplorer/Resources/Public/Icons/';
		$cnt=0;
		if (is_array($list)) $cnt = count($list);
		for($i=0;$i<$cnt;++$i) {
			$dir = $list[$i][0];	$item = $list[$i][1];
			$s_dir=GeneralUtility::fixed_lgd_cs($dir,62);
			$s_item=GeneralUtility::fixed_lgd_cs($item,45);
			$link = '';	$target = '';
			
			if(quickplorer_div::get_is_dir($dir,$item)) {
				$img = 'folder.gif';
				$link = quickplorer_div::make_link('list',quickplorer_div::get_rel_item($dir, $item),NULL);
			} else {
				$img = quickplorer_div::get_mime_type($dir, $item, 'img');
				$link = $GLOBALS['T3Q_VARS']['home_url'].'/'.quickplorer_div::get_rel_item($dir, $item);
				$target = '_blank';
			}
			
			$this->contents[]='
				<tr>
				  <td>
				    <img border="0" align="absmiddle" '.IconUtility::skinImg($BACK_PATH,$imagepath.$img,'width="16" height="16"',0).' alt="">&nbsp;
				    <a href="'.$link.'" target="'.$target.'">'.$s_item.'</a>
				  </td>
				  <td>
				    <a href="'.quickplorer_div::make_link('list',$dir,NULL).'"> /'.$s_dir.'</a>
				  </td>
				</tr>';
		}
	}
	//------------------------------------------------------------------------------
	function main($dir) {			// search for item

		$this->contents = array();
		
		
	
		if(GeneralUtility::_POST('searchitem')) {
			//$searchitem=preg_replace('/[^a-z0-9_\.\*\-\?]*/i','',GeneralUtility::_POST('searchitem'));
			$searchitem = htmlspecialchars(GeneralUtility::_POST('searchitem'));
			$subdir=GeneralUtility::_POST('subdir')=='y';
			$list=$this->make_list($dir,$searchitem,$subdir);
		} else {
			$searchitem=NULL;
			$subdir=true;
		}
		
		$msg='';
		//if($searchitem!=NULL) $msg.=': (/' .quickplorer_div::get_rel_item($dir, $searchitem).')';
		//show_header($msg);
		
		// Search Box
		$this->contents[]=' 
			<br />
			  <table>
			    <form name="searchform" action="'.quickplorer_div::make_link('search',$dir,NULL).'" method="post">
				<tr>
				  <td>
				    <input name="searchitem" type="text" size="25" value="'.$searchitem.'">
					<input type="submit" value="'.$GLOBALS['LANG']->getLL('message.btnsearch').'">&nbsp;
					<input type="button" value="'.$GLOBALS['LANG']->getLL('message.btnclose').'" onClick="javascript:location=\''.quickplorer_div::make_link('list',$dir,NULL).'\';">
				  </td>
				</tr>
				<tr>
				  <td>
				    <input type="checkbox" name="subdir" value="y"'.($subdir?" checked>":">").$GLOBALS['LANG']->getLL('message.miscsubdirs').'
				  </td>
				</tr>
				</form>
			  </table>';
		
		// Results
		if($searchitem!=NULL) {
			$this->contents[]='
				<table width="100%" id="typo3-filelist">
				  <tr>
				    <td colspan="2"> </td>
				  </tr>';
				  
			if(is_array($list) && count($list)>0) {
				// Table Header
				$this->contents[]='
					<tr>
					  <td width="45%" class="c-headLine">'.$GLOBALS['LANG']->getLL('message.nameheader').'</td>
					  <td width="60%" class="c-headLine">'.$GLOBALS['LANG']->getLL('message.pathheader').'</td>
					</tr>
					<tr>
					  <td colspan="2"> </td>
					</tr>';
		
				// make & print table of found items
				$this->print_table($list);
	
				$this->contents[]='
					<tr>
					  <td colspan="2"> </td>
					</tr>
					<tr>
					  <td class="c-headLine">'.count($list).' '.$GLOBALS['LANG']->getLL('message.miscitems').'</td>
					  <td class="c-headLine"> </td>
					</tr>
					';
			} else {
				$this->contents[]='
				   <tr>
				     <td><br /><br />'.$GLOBALS['LANG']->getLL('message.miscnoresult').'</td>
				   </tr>';
			}
			$this->contents[]='
				   <tr>
				     <td colspan="2">  </TD>
				   </tr>
				 </table>';
		}
		
		return implode('',$this->contents);
	}
}

if (defined('TYPO3') && $TYPO3_CONF_VARS['BE']['XCLASS']['ext/quickplorer/Classes/Controller/quickplorer_search.php'])	{
	include_once($TYPO3_CONF_VARS['BE']['XCLASS']['ext/quickplorer/Classes/Controller/quickplorer_search.php']);
}

?>