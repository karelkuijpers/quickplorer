<?php
namespace Parousia\Quickplorer\Controller;

/***************************************************************
*  Copyright notice
*  
*  (c) 2004 Mads Brunn (madsbrunn@gmail.com)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is 
*  free software; you can redistribute it and/or modify
					
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
* 
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
* 
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
					
/***************************************************************

     The Original Code is index.php, released on 2003-04-02.

     The Initial Developer of the Original Code is The QuiX project.
	 f
	 quix@free.fr
	 http://www.quix.tk
	 http://quickplorer.sourceforge.net

****************************************************************/
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
//USE TYPO3\CMS\Backend\Module\BaseScriptClass;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Backend\Routing\UriBuilder;
use TYPO3\CMS\Backend\Routing\Router;
use TYPO3\CMS\Core\Http\ServerRequest;
use Psr\Http\Message\ResponseInterface;
use Parousia\Quickplorer\Hooks\quickplorer_div;
use Parousia\Quickplorer\Controller\quickplorer_listdir;
use Parousia\Quickplorer\Controller\quickplorer_edit;
use Parousia\Quickplorer\Controller\quickplorer_extract;
use Parousia\Quickplorer\Controller\quickplorer_archive;
use Parousia\Quickplorer\Controller\quickplorer_copymove;
use Parousia\Quickplorer\Controller\quickplorer_rename;
use Parousia\Quickplorer\Controller\quickplorer_chmod;
use Parousia\Quickplorer\Controller\quickplorer_delete;
use Parousia\Quickplorer\Controller\quickplorer_mkitem;
use Parousia\Quickplorer\Controller\quickplorer_upload;
use Parousia\Quickplorer\Controller\quickplorer_download;
use Parousia\Quickplorer\Controller\quickplorer_search;
//use TYPO3\CMS\Backend\Template\DocumentTemplate;
use TYPO3\CMS\Backend\Template\ModuleTemplate;
use TYPO3\CMS\Core\Type\Bitmask\Permission;


	
/** 	
 * Module 'quickplorer' for the 'quickplorer' extension.
 *
 * @author	Karel Kuijpers <karelkuijpers@gmail.com>
 */
	


	// DEFAULT initialization of a module [BEGIN]
	// DEFAULT initialization of a module [END]


//class quickplorer extends BaseScriptClass {
class quickplorer {

	protected $BACK_PATH = '';

    /**
     * Generally used for accumulating the output content of backend modules
     *
     * @var string
     */

	public $contents = '';
    /**
     * @var \TYPO3\CMS\Backend\Template\ModuleTemplate
     */
    public $docs;
	
    /**
     * @var \TYPO3\CMS\Core\Page\PageRenderer;
     */
    public $pageRenderer;
	

    /**
     * Loaded with the global array $MCONF which holds some module configuration from the conf.php file of backend modules.
     *
     * @see init()
     * @var array
     */
    protected $MCONF = [];

    /**
     * The integer value of the GET/POST var, 'id'. Used for submodules to the 'Web' module (page id)
     *
     * @see init()
     * @var int
     */
    protected $id;

    /**
     * The value of GET/POST var, 'CMD'
     *
     * @see init()
     * @var mixed
     */
    protected $CMD;

    /**
     * A WHERE clause for selection records from the pages table based on read-permissions of the current backend user.
     *
     * @see init()
     * @var string
     */
    protected $perms_clause;

    /**
     * The name of the module
     *
     * @var string
     */
    protected $moduleName = 'tools_quickplorer';

    /**
     * The module menu items array. Each key represents a key for which values can range between the items in the array of that key.
     *
     * @see init()
     * @var array
     */
    protected $MOD_MENU = [
        'function' => []
    ];

    /**
     * Current settings for the keys of the MOD_MENU array
     *
     * @see $MOD_MENU
     * @var array
     */
    protected $MOD_SETTINGS = [];

    /**
     * Module TSconfig based on PAGE TSconfig / USER TSconfig
     *
     * @see menuConfig()
     * @var array
     */
    protected $modTSconfig;

    /**
     * If type is 'ses' then the data is stored as session-lasting data. This means that it'll be wiped out the next time the user logs in.
     * Can be set from extension classes of this class before the init() function is called.
     *
     * @see menuConfig(), \TYPO3\CMS\Backend\Utility\BackendUtility::getModuleData()
     * @var string
     */
    protected $modMenu_type = '';

	var $routes = array();
	/**
	 * Initializes the module. Basically just calls parent::init
	 * Initialize language files
 	 *
	 * @return	void
	 */
	 
    /**
     * dontValidateList can be used to list variables that should not be checked if their value is found in the MOD_MENU array. Used for dynamically generated menus.
     * Can be set from extension classes of this class before the init() function is called.
     *
     * @see menuConfig(), \TYPO3\CMS\Backend\Utility\BackendUtility::getModuleData()
     * @var string
     */
    protected $modMenu_dontValidateList = '';

    /**
     * List of default values from $MOD_MENU to set in the output array (only if the values from MOD_MENU are not arrays)
     * Can be set from extension classes of this class before the init() function is called.
     *
     * @see menuConfig(), \TYPO3\CMS\Backend\Utility\BackendUtility::getModuleData()
     * @var string
     */
    protected $modMenu_setDefaultList = '';

    /**
     * Contains module configuration parts from TBE_MODULES_EXT if found
     *
     * @see handleExternalFunctionValue()
     * @var array
     */
    /**
     * Contains module configuration parts from TBE_MODULES_EXT if found
     *
     * @see handleExternalFunctionValue()
     * @var array
     */
    protected $extClassConf;
	

    /**
     */
//    public function init()
    public function __construct()
    {

//       	$GLOBALS['SOBE'] = $this;
		// Include the LL file 
		$GLOBALS['LANG']->includeLLFile('EXT:quickplorer/Resources/Private/Language/locallang.xml');
        $this->MCONF['name'] = $this->moduleName;
//  		parent::init();   }
        $this->id = (int)GeneralUtility::_GP('id');
        $this->CMD = GeneralUtility::_GP('CMD');
        $this->perms_clause = $this->getBackendUser()->getPagePermsClause(Permission::PAGE_SHOW);
        $this->menuConfig();
        $this->handleExternalFunctionValue();
	}

	/**
	 * Main function of the module. Write the content to $this->contents
	 */
	function mainProcess()
	{
        // Setting module configuration:


		$this->BACK_PATH = '../../../../typo3/';
//		$GLOBALS['LANG']->includeLLFile('EXT:quickplorer/Resources/Private/Language/locallang.xml');
		$GLOBALS['T3Q_VARS'] = array();
		$extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('quickplorer');					
//		if (is_array($extensionConfiguration))error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'Quickplorer configuration extensionConfiguration: '.GeneralUtility::array2xml($extensionConfiguration)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/quickplorer/Classes/Controller/debug.log');

		if (is_array($extensionConfiguration)) $GLOBALS['T3Q_VARS'] = array_merge($GLOBALS['T3Q_VARS'],$extensionConfiguration);

		if(!strlen(trim($GLOBALS['T3Q_VARS']['editable_ext']))){
			$GLOBALS['T3Q_VARS']['editable_ext'] = "\.phpcron$|\.ts$|\.tmpl$|\.txt$|\.php$|\.php3$|\.phtml$|\.inc$|\.sql$|\.pl$|\.htm$|\.html$|\.shtml$|\.dhtml$|\.xml$|\.js$|\.css$|\.cgi$|\.cpp$\.c$|\.cc$|\.cxx$|\.hpp$|\.h$|\.pas$|\.p$|\.java$|\.py$|\.sh$\.tcl$|\.tk$";
		} 

		
//		if(!strlen(trim($GLOBALS['T3Q_VARS']['home_dir']))){
			$GLOBALS["T3Q_VARS"]["home_dir"] = preg_replace('/\/$/','',Environment::getPublicPath() . '/');
//		}

//		if(!strlen(trim($GLOBALS['T3Q_VARS']['home_url']))){
			$GLOBALS['T3Q_VARS']['home_url'] = GeneralUtility::getIndpEnv('TYPO3_REQUEST_HOST');
//		}
		
		if(!isset($GLOBALS['T3Q_VARS']['module_url'])){
			$uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);
			$GLOBALS['T3Q_VARS']['module_url'] = $uriBuilder->buildUriFromRoute($this->MCONF['name']);
//			$GLOBALS['T3Q_VARS']['module_url'] = BackendUtility::getModuleUrl($this->MCONF['name']);
		}

		$GLOBALS['T3Q_VARS']['super_mimes'] = array(
			// dir, exe, file
			'dir'	=> array($GLOBALS['LANG']->getLL('mime.dir'),'folder.gif'),
			'exe'	=> array($GLOBALS['LANG']->getLL('mime.exe'),'exe.gif',"\.exe$|\.com$|\.bin$"),
			'file'	=> array($GLOBALS['LANG']->getLL('mime.file'),'default.gif')
		);
		$GLOBALS['T3Q_VARS']['used_mime_types'] = array(
			// text
			'text'	=> array($GLOBALS['LANG']->getLL('mime.text'),'txt.gif',"\.txt$"),
			
			// programming
			'php'	=> array($GLOBALS['LANG']->getLL('mime.php'),'php.gif',"\.php$|\.php3$|\.phtml$|\.inc$"),
			'sql'	=> array($GLOBALS['LANG']->getLL('mime.sql'),'src.gif',"\.sql$"),
			'perl'	=> array($GLOBALS['LANG']->getLL('mime.perl'),'pl.gif',"\.pl$"),
			'html'	=> array($GLOBALS['LANG']->getLL('mime.html'),'html.gif',"\.htm$|\.html$|\.shtml$|\.dhtml$|\.xml$"),
			'js'	=> array($GLOBALS['LANG']->getLL('mime.js'),'js.gif',"\.js$"),
			'css'	=> array($GLOBALS['LANG']->getLL('mime.css'),'src.gif',"\.css$"),
			'cgi'	=> array($GLOBALS['LANG']->getLL('mime.cgi'),'exe.gif',"\.cgi$"),
			//'py'	=> array($GLOBALS['LANG']->getLL('mime.py'),'py.gif',"\.py$"),
			//'sh'	=> array($GLOBALS['LANG']->getLL('mime.sh'),'sh.gif',"\.sh$"),
			// C++
			'cpps'	=> array($GLOBALS['LANG']->getLL('mime.cpps'),'cpp.gif',"\.cpp$|\.c$|\.cc$|\.cxx$"),
			'cpph'	=> array($GLOBALS['LANG']->getLL('mime.cpph'),'h.gif',"\.hpp$|\.h$"),
			// Java
			'javas'	=> array($GLOBALS['LANG']->getLL('mime.javas'),'java.gif',"\.java$"),
			'javac'	=> array($GLOBALS['LANG']->getLL('mime.javac'),'java.gif',"\.class$|\.jar$"),
			// Pascal
			'pas'	=> array($GLOBALS['LANG']->getLL('mime.pas'),'src.gif',"\.p$|\.pas$"),
			
			// images
			'gif'	=> array($GLOBALS['LANG']->getLL('mime.gif'),'image.gif',"\.gif$"),
			'jpg'	=> array($GLOBALS['LANG']->getLL('mime.jpg'),'image.gif',"\.jpg$|\.jpeg$"),
			'bmp'	=> array($GLOBALS['LANG']->getLL('mime.bmp'),'image.gif',"\.bmp$"),
			'png'	=> array($GLOBALS['LANG']->getLL('mime.png'),'image.gif',"\.png$"),
			
			// compressed
			'zip'	=> array($GLOBALS['LANG']->getLL('mime.zip'),'zip.gif',"\.zip$"),
			'tar'	=> array($GLOBALS['LANG']->getLL('mime.tar'),'tar.gif',"\.tar$"),
			'gzip'	=> array($GLOBALS['LANG']->getLL('mime.gzip'),'tgz.gif',"\.tgz$|\.gz$"),
			'bzip2'	=> array($GLOBALS['LANG']->getLL('mime.bzip2'),'tgz.gif',"\.bz2$"),
			'rar'	=> array($GLOBALS['LANG']->getLL('mime.rar'),'tgz.gif',"\.rar$"),
			//'deb'	=> array($GLOBALS['LANG']->getLL('mime.deb'),'package.gif',"\.deb$"),
			//'rpm'	=> array($GLOBALS['LANG']->getLL('mime.rpm'),'package.gif',"\.rpm$"),
			
			// music
			'mp3'	=> array($GLOBALS['LANG']->getLL('mime.mp3'),'mp3.gif',"\.mp3$"),
			'wav'	=> array($GLOBALS['LANG']->getLL('mime.wav'),'sound.gif',"\.wav$"),
			'midi'	=> array($GLOBALS['LANG']->getLL('mime.midi'),'midi.gif',"\.mid$"),
			'real'	=> array($GLOBALS['LANG']->getLL('mime.real'),'real.gif',"\.rm$|\.ra$|\.ram$"),
			//'play'	=> array($GLOBALS['LANG']->getLL('mime.play'),'mp3.gif',"\.pls$|\.m3u$"),
			
			// movie
			'mpg'	=> array($GLOBALS['LANG']->getLL('mime.mpg'),'video.gif',"\.mpg$|\.mpeg$"),
			'mov'	=> array($GLOBALS['LANG']->getLL('mime.mov'),'video.gif',"\.mov$"),
			'avi'	=> array($GLOBALS['LANG']->getLL('mime.avi'),'video.gif',"\.avi$"),
			'flash'	=> array($GLOBALS['LANG']->getLL('mime.flash'),'flash.gif',"\.swf$"),
			
			// Micosoft / Adobe
			'word'	=> array($GLOBALS['LANG']->getLL('mime.word'),'word.gif',"\.doc$"),
			'excel'	=> array($GLOBALS['LANG']->getLL('mime.excel'),'spread.gif',"\.xls$"),
			'pdf'	=> array($GLOBALS['LANG']->getLL('mime.pdf'),'pdf.gif',"\.pdf$")
		);
		
				
		//general date format used in listings
		$GLOBALS['T3Q_VARS']['date_fmt'] = 'd/m/Y H:i';
		$GLOBALS['T3Q_VARS']['moduleData'] = $this->MOD_SETTINGS;
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'mod mconf:'.GeneralUtility::array2xml($MCONF)."\r\n",3,'quickplorer.txt');

		if(GeneralUtility::_GET('action')) $GLOBALS['T3Q_VARS']['action']=GeneralUtility::_GET('action');
			else $GLOBALS['T3Q_VARS']['action']='list';
		if($GLOBALS['T3Q_VARS']['action']=="post" && GeneralUtility::_POST('do_action')) {
			$GLOBALS['T3Q_VARS']['action']=GeneralUtility::_POST('do_action');
		}
		if($GLOBALS['T3Q_VARS']['action']=='') $GLOBALS['T3Q_VARS']['action']='list';


		if(GeneralUtility::_POST('jumptodir')){
			 $GLOBALS['T3Q_VARS']['dir']=GeneralUtility::_POST('jumpdir');
		} elseif(GeneralUtility::_GET('dir')){
			$GLOBALS['T3Q_VARS']['dir'] = GeneralUtility::_GET('dir');
			
		} elseif(isset($GLOBALS['T3Q_VARS']['moduleData']['dir']) && !isset($_GET['dir'])){
			 $GLOBALS['T3Q_VARS']['dir']=$GLOBALS['T3Q_VARS']['moduleData']['dir'];
		}
		else $GLOBALS['T3Q_VARS']['dir'] = '';

		if($GLOBALS['T3Q_VARS']['dir']=='.') $GLOBALS['T3Q_VARS']['dir']='';

		$GLOBALS['T3Q_VARS']['moduleData']['dir'] =  $GLOBALS['T3Q_VARS']['dir'];
		
		if(GeneralUtility::_GET('item')) $GLOBALS['T3Q_VARS']['item']=GeneralUtility::_GET('item');
		else $GLOBALS['T3Q_VARS']['item']='';

		if(GeneralUtility::_GET('order')) $GLOBALS['T3Q_VARS']['order'] = GeneralUtility::_GET('order');
		else $GLOBALS['T3Q_VARS']['order']='name';
		if($GLOBALS['T3Q_VARS']['order']=='') $GLOBALS['T3Q_VARS']['order'] = 'name';
		
		if(GeneralUtility::_GET('srt')) $GLOBALS['T3Q_VARS']['srt']=GeneralUtility::_GET('srt');
		else $GLOBALS['T3Q_VARS']['srt']='yes';
		if($GLOBALS['T3Q_VARS']['srt']=='') $GLOBALS['T3Q_VARS']['srt']=='yes';

			// Draw the header.
		$this->docs = GeneralUtility::makeInstance(ModuleTemplate::class);
		$this->pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
		$this->pageRenderer->addCssFile(PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('quickplorer')).'Resources/Public/Css/layout.css',$relation = 'stylesheet','screen', $title = 'quickplorer');
		
		# Render content:
		# NOTE NOTE NOTE
		# $this->pageRenderer->JScode is set in $this->moduleContent()
		# therefore it is very important that $this->moduleContent()
		# is called before start of Page and that the output
		# of this function is added to $this->contents AFTER that
		
		$mainsection = $this->moduleContent();
		$this->contents .= '<a name="top"></a>';		
		$this->pageRenderer->setTitle($GLOBALS['LANG']->getLL('title'));	
		$this->contents .= $this->pageRenderer->render();
		$this->contents .= '<h1 class="t3js-title-inlineedit">'.htmlspecialchars($GLOBALS['LANG']->getLL('title')).'</h1>';
		if (isset($mainsection['header']))$this->contents .=  htmlspecialchars($mainsection['header']);
		if (isset($mainsection['content']))$this->contents .= '
      <!-- Section content -->
  ' . $mainsection['content'];
		
		$this->getBackendUser()->pushModuleData('tools_quickplorer',$GLOBALS['T3Q_VARS']['moduleData']);
	}

	/**
	 * Prints out the module HTML
	 */
	function printContent()	{

		echo $this->contents;
	}
	
	/**
	 * Generates the module content
	*/
	function moduleContent()
	{
		$section = array();
		if (isset($GLOBALS['T3Q_ERRORS']))
			$GLOBALS['T3Q_DEBUG']['initial_errors']= count($GLOBALS['T3Q_ERRORS']);
		else $GLOBALS['T3Q_DEBUG']['initial_errors']= 0;
		switch ($GLOBALS['T3Q_VARS']['action']){

			case 'edit':
				$editObj = GeneralUtility::makeInstance(quickplorer_edit::class);
				$section['header'] = $GLOBALS['LANG']->getLL('message.actedit');
				$section['content'] = $editObj->main($GLOBALS['T3Q_VARS']['dir'],$GLOBALS['T3Q_VARS']['item'],$this);
				break;

			case 'extract':
				$extractObj = GeneralUtility::makeInstance(quickplorer_extract::class);
				$section['header'] = $GLOBALS['LANG']->getLL('message.actextract');
				$section['content'] = $extractObj->main($GLOBALS['T3Q_VARS']['dir'],$GLOBALS['T3Q_VARS']['item'],$this);
				break;

			case 'arch':
				$archObj = GeneralUtility::makeInstance(quickplorer_archive::class);
				$section['header'] = $GLOBALS['LANG']->getLL('message.actarchive');
				$section['content'] = $archObj->main($GLOBALS['T3Q_VARS']['dir'],$this);
				break;
				
			case 'copy':	
			case 'move':
			case 'paste':
			case 'clear':
				$copymoveObj = GeneralUtility::makeInstance(quickplorer_copymove::class);
				$copymoveObj->main($GLOBALS['T3Q_VARS']['dir'],$this);
				break;

			case 'rename':
				$renameObj = GeneralUtility::makeInstance(quickplorer_rename::class);
				$section['header'] = $GLOBALS['LANG']->getLL('message.actrename');
				$section['content'] = $renameObj->main($GLOBALS['T3Q_VARS']['dir'],$GLOBALS['T3Q_VARS']['item'],$this);
				break;

			case 'chmod':
				$chmodObj = GeneralUtility::makeInstance(quickplorer_chmod::class);
				$section['header'] = $GLOBALS['LANG']->getLL('message.actperms');
				$section['content'] = $chmodObj->main($GLOBALS['T3Q_VARS']['dir'],$GLOBALS['T3Q_VARS']['item'],$this);
				break;
				
			case 'delete':
				$deleteObj = GeneralUtility::makeInstance(quickplorer_delete::class);
				$deleteObj->main($GLOBALS['T3Q_VARS']['dir'],$this); //no content added
				break;
				
			case 'mkitem':
				$mkitemObj = GeneralUtility::makeInstance(quickplorer_mkitem::class);
				$mkitemObj->main($GLOBALS['T3Q_VARS']['dir'],$this); //no content added
				break;

			case 'upload':
				$uploadObj = GeneralUtility::makeInstance(quickplorer_upload::class);
				$section['header'] = $GLOBALS['LANG']->getLL('message.actupload');
				$section['content'] = $uploadObj->main($GLOBALS['T3Q_VARS']['dir'],$this);
				break;
				
			case 'download':
				$downloadObj = GeneralUtility::makeInstance(quickplorer_download::class);
				$downloadObj->main($GLOBALS['T3Q_VARS']['dir'],$GLOBALS['T3Q_VARS']['item'],$this);
				break;
			
			case 'search':
				$searchObj = GeneralUtility::makeInstance(quickplorer_search::class);
				$section['header'] = $GLOBALS['LANG']->getLL('message.actsearch');
				$section['content'] = $searchObj->main($GLOBALS['T3Q_VARS']['dir'],$this);
				break;

				
			case 'list':
			default:
				$listdirObj = GeneralUtility::makeInstance(quickplorer_listdir::class);
				$section['header'] = '';
				$section['content'] = $listdirObj->main($GLOBALS['T3Q_VARS']['dir'],$this);
				break;

		}

		return $section;

	}
	 
    /**
     * Injects the request object for the current request and gathers all data
     *
     * BROWSING FILES:
     *
     * Incoming array has syntax:
     * GETvar 'id' = import page id (must be readable)
     *
     * file = 	(pointing to filename relative to PATH_site)
     *
     * [all relation fields are clear, but not files]
     * - page-tree is written first
     * - then remaining pages (to the root of import)
     * - then all other records are written either to related included pages or if not found to import-root (should be a sysFolder in most cases)
     * - then all internal relations are set and non-existing relations removed, relations to static tables preserved.
     *
     * @param ServerRequestInterface $request the current request
     * @param ResponseInterface $response
     * @return ResponseInterface the response with the content
     */
    public function mainAction(ServerRequest $request):ResponseInterface
    {
		$response = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Http\Response::class);
     //   $GLOBALS['SOBE'] = $this;
//        $this->init();
        $this->mainProcess();
        $response->getBody()->write($this->contents);
        return $response;

    }
    /**
     * Initializes the internal MOD_MENU array setting and unsetting items based on various conditions. It also merges in external menu items from the global array TBE_MODULES_EXT (see mergeExternalItems())
     * Then MOD_SETTINGS array is cleaned up (see \TYPO3\CMS\Backend\Utility\BackendUtility::getModuleData()) so it contains only valid values. It's also updated with any SET[] values submitted.
     * Also loads the modTSconfig internal variable.
     *
     * @see init(), $MOD_MENU, $MOD_SETTINGS, \TYPO3\CMS\Backend\Utility\BackendUtility::getModuleData(), mergeExternalItems()
     */
    protected function menuConfig()
    {
        // Page / user TSconfig settings and blinding of menu-items
        $this->modTSconfig['properties'] = BackendUtility::getPagesTSconfig($this->id)['mod.'][$this->MCONF['name'] . '.'] ?? [];
        $this->MOD_MENU['function'] = $this->mergeExternalItems($this->MCONF['name'], 'function', $this->MOD_MENU['function']);
        $blindActions = $this->modTSconfig['properties']['menu.']['function.'] ?? [];
        foreach ($blindActions as $key => $value) {
            if (!$value && array_key_exists($key, $this->MOD_MENU['function'])) {
                unset($this->MOD_MENU['function'][$key]);
            }
        }
        $this->MOD_SETTINGS = BackendUtility::getModuleData($this->MOD_MENU, GeneralUtility::_GP('SET'), $this->MCONF['name'], $this->modMenu_type, $this->modMenu_dontValidateList, $this->modMenu_setDefaultList);
    }


    /**
     * Loads $this->extClassConf with the configuration for the CURRENT function of the menu.
     *
     * @param string $MM_key The key to MOD_MENU for which to fetch configuration. 'function' is default since it is first and foremost used to get information per "extension object" (I think that is what its called)
     * @param string $MS_value The value-key to fetch from the config array. If NULL (default) MOD_SETTINGS[$MM_key] will be used. This is useful if you want to force another function than the one defined in MOD_SETTINGS[function]. Call this in init() function of your Script Class: handleExternalFunctionValue('function', $forcedSubModKey)
     * @see getExternalItemConfig(), init()
     */
    protected function handleExternalFunctionValue($MM_key = 'function', $MS_value = null)
    {
        if ($MS_value === null) {
            $MS_value = $this->MOD_SETTINGS[$MM_key];
        }
        $this->extClassConf = $this->getExternalItemConfig($this->MCONF['name'], $MM_key, $MS_value);
    }

    /**
     * Returns configuration values from the global variable $TBE_MODULES_EXT for the module given.
     * For example if the module is named "web_info" and the "function" key ($menuKey) of MOD_SETTINGS is "stat" ($value) then you will have the values of $TBE_MODULES_EXT['webinfo']['MOD_MENU']['function']['stat'] returned.
     *
     * @param string $modName Module name
     * @param string $menuKey Menu key, eg. "function" for the function menu. See $this->MOD_MENU
     * @param string $value Optionally the value-key to fetch from the array that would otherwise have been returned if this value was not set. Look source...
     * @return mixed The value from the TBE_MODULES_EXT array.
     * @see handleExternalFunctionValue()
     */
    public function getExternalItemConfig($modName, $menuKey, $value = '')
    {
        if (isset($GLOBALS['TBE_MODULES_EXT'][$modName])) {
            return (string)$value !== '' ? $GLOBALS['TBE_MODULES_EXT'][$modName]['MOD_MENU'][$menuKey][$value] : $GLOBALS['TBE_MODULES_EXT'][$modName]['MOD_MENU'][$menuKey];
        }
        return null;
    }
    /**
     * Returns the Backend User
     * @return BackendUserAuthentication
     */
    protected function getBackendUser()
    {
        return $GLOBALS['BE_USER'];
    }

    /**
     * Merges menu items from global array $TBE_MODULES_EXT
     *
     * @param string $modName Module name for which to find value
     * @param string $menuKey Menu key, eg. 'function' for the function menu.
     * @param array $menuArr The part of a MOD_MENU array to work on.
     * @return array Modified array part.
     * @internal
     * @see \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::insertModuleFunction(), menuConfig()
     */
    protected function mergeExternalItems($modName, $menuKey, $menuArr)
    {
/*        $mergeArray = $GLOBALS['TBE_MODULES_EXT'][$modName]['MOD_MENU'][$menuKey];
        if (is_array($mergeArray)) {
            foreach ($mergeArray as $k => $v) {
                if (((string)$v['ws'] === '' || $this->getBackendUser()->workspace === 0 && GeneralUtility::inList($v['ws'], 'online')) || $this->getBackendUser()->workspace === -1 && GeneralUtility::inList($v['ws'], 'offline') || $this->getBackendUser()->workspace > 0 && GeneralUtility::inList($v['ws'], 'custom')) {
                    $menuArr[$k] = $this->getLanguageService()->sL($v['title']);
                }
            }
        } */
        return $menuArr;
    }

    /**
     * Returns the Language Service
     * @return LanguageService
     */
    protected function getLanguageService()
    {
        return $GLOBALS['LANG'];
    }


}
