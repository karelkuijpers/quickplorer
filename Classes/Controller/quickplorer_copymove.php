<?php
namespace Parousia\Quickplorer\Controller;
/***************************************************************
*  Copyright notice
*  
*  (c) 2004 Mads Brunn (brunn@mail.dk)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is 
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
* 
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
* 
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/** 
 * class 'quickplorer_copymove' for the 'quickplorer' extension.
 * Class for copy-move functionality
 *
 * @author	Mads Brunn <brunn@mail.dk>
 */
/***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\File\BasicFileUtility;
use Parousia\Quickplorer\Hooks\quickplorer_div;

class quickplorer_copymove{


	function main($dir){
		switch($GLOBALS["T3Q_VARS"]["action"]){

			case "copy":
			case "move":
			
				$selitems = GeneralUtility::_POST("selitems");
				if (is_array($selitems))$cnt=count($selitems); else $cnt=0;
				//$cnt=count($selitems);
				if($cnt<1){
					setCookie("copymoveitems",serialize(array()),0,'/');
					setCookie("action","",0,'/');
					setCookie("dir","",0,'/');
					header("Location: ".quickplorer_div::make_link("list",$dir,NULL));
				}

				$copymoveitems = array();
				for($i=0;$i<$cnt;++$i) {
					$copymoveitems[] = quickplorer_div::get_abs_item($dir,$selitems[$i]);
				}
				setCookie("copymoveitems",serialize($copymoveitems),0,'/');
				setCookie("action",$GLOBALS["T3Q_VARS"]["action"],0,'/');
				setCookie("dir",$dir,0,'/');
				
				break;


			case "paste":
				$basicFileObj = GeneralUtility::makeInstance(BasicFileUtility::class);
			
				$action = $_COOKIE["action"];
				$cmitems = unserialize(stripslashes($_COOKIE["copymoveitems"]));
				$old_dir = $_COOKIE["dir"];
				if (is_array($cmitems))$cnt=count($cmitems); else $cnt=0;
				
				//$cnt=count($cmitems);

				if(!@file_exists(quickplorer_div::get_abs_dir($dir))) quickplorer_div::showError($dir.": ".$GLOBALS['LANG']->getLL("error.targetexist"));
				if(!quickplorer_div::get_show_item($dir,"")) quickplorer_div::showError($dir.": ".$GLOBALS['LANG']->getLL("error.accesstarget"));
				if(!quickplorer_div::down_home(quickplorer_div::get_abs_dir($dir))) quickplorer_div::showError($dir.": ".$GLOBALS['LANG']->getLL("error.targetabovehome"));
				
				// copy / move files
				$err=false;
				for($i=0;$i<$cnt;++$i) {
					//$tmp = stripslashes($GLOBALS['__POST']["selitems"][$i]);
					//$new = basename(stripslashes($GLOBALS['__POST']["newitems"][$i]));
					$abs_item = $cmitems[$i];
					$abs_new_item = quickplorer_div::get_abs_item($dir,basename($abs_item));
					$items[$i] = basename($abs_item);
				
					// Check
					/*if($new=="") {
						$error[$i]= $GLOBALS['LANG']->getLL("error.miscnoname");
						$err=true;	continue;
					}*/
					if(!@file_exists($abs_item)) {
						$error[$i]= $GLOBALS['LANG']->getLL("error.itemexist");
						$err=true;	continue;
					}
					if(!quickplorer_div::get_show_item($old_dir, $tmp)) {
						$error[$i]= $GLOBALS['LANG']->getLL("error.accessitem");
						$err=true;	continue;
					}
					if(@file_exists($abs_new_item)) {
						
						//$error[$i]= $GLOBALS['LANG']->getLL("error.targetdoesexist");
						//$err=true;	continue;
						
						# Changed handling of files that already exists in destination folder
						# Instead of isssuing an error the file is renamed
						$abs_new_item = $basicFileObj->getUniqueName(basename($abs_new_item),dirname($abs_new_item));
					}
				
					// Copy / Move
					if($action=="copy") {
						if(@is_link($abs_item) || @is_file($abs_item)) {
							// check file-exists to avoid error with 0-size files (PHP 4.3.0)
							$ok=@copy($abs_item,$abs_new_item);	//||@file_exists($abs_new_item);
						} elseif(@is_dir($abs_item)) {
							$ok=quickplorer_div::copy_dir($abs_item,$abs_new_item);
						}
					} else {
						$ok=@rename($abs_item,$abs_new_item);
					}
					
					if($ok===false) {
						$error[$i]=($action=="copy"?$GLOBALS['LANG']->getLL("error.copyitem"):$GLOBALS['LANG']->getLL("error.moveitem"));
						$err=true;	continue;
					}
					
					$error[$i]=NULL;
				}
				
				if($err) {			// there were errors
					$err_msg="";
					for($i=0;$i<$cnt;++$i) {
						if($error[$i]==NULL) continue;
						
						$err_msg .= $items[$i]." : ".$error[$i]."<br />";
					}
					quickplorer_div::showError($err_msg);
				}

				setCookie("copymoveitems",serialize(array()),0,'/');
				setCookie("action","",0,'/');
				setCookie("dir","",0,'/');
			
				break;
			
			case "clear":
			default:
				setCookie("copymoveitems",serialize(array()),0,'/');
				setCookie("action","",0,'/');
				setCookie("dir","",0,'/');
				break;
		}
		
		header("Location: ".quickplorer_div::make_link("list",$dir,NULL));
	}
}
