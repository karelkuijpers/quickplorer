<?php
namespace Parousia\Quickplorer\Controller;
/***************************************************************
*  Copyright notice
*  
*  (c) 2004 Mads Brunn (madsbrunn@gmail.com)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is 
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
* 
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
* 
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/* 
 * class 'quickplorer_archive' for the 'quickplorer' extension.
 * Contains functions to create an archive.
 *
 * @author	Karel Kuijpers <karelkuijpers@gmail.com>
 */
/***************************************************************

     The Original Code is fun_archive.php, released on 2003-03-31.

     The Initial Developer of the Original Code is The QuiX project.
	 
	 quix@free.fr
	 http://www.quix.tk
	 http://quickplorer.sourceforge.net

****************************************************************/
use TYPO3\CMS\Core\Utility\GeneralUtility;
use Parousia\Quickplorer\Hooks\quickplorer_div;
use Parousia\Quickplorer\Controller\quickplorer_zipfile;


class quickplorer_archive{


	function main($dir) {
		$content = array();
		
		
		if(GeneralUtility::_POST('name')) {
			$name=basename(stripslashes(GeneralUtility::_POST('name')));
			if($name=='') quickplorer_div::showError($GLOBALS['LANG']->getLL('error.miscnoname'));
			switch(GeneralUtility::_POST('type')) {
				case 'zip':	
					$this->zip_items($dir,$name);	
					break;
				default:	
					$this->zip_items($dir,$name);
					break;
			}
			header('Location: '.quickplorer_div::make_link('list',$dir,NULL));
		}
		
		$content[] = '
			<br />
			  <form name="archform" method="post" action="'.quickplorer_div::make_link('arch',$dir,NULL).'">
			 ';
		
		$selitems = GeneralUtility::_POST('selitems');
		
		if (is_array($selitems))$cnt=count($selitems); else $cnt=0;
		for($i=0;$i<$cnt;++$i) {
			$content[] = '<input type="hidden" name="selitems[]" value="'.stripslashes($selitems[$i]).'">';
		}
		
		$content[] = '
			<table width="300">
			  <tr>
			    <td>'.$GLOBALS['LANG']->getLL('message.nameheader').':</td>
				<td align="right"><input type="text" name="name" size="25"></td>
			  </tr>
			  <tr>
			    <td>'.$GLOBALS['LANG']->getLL('message.typeheader').':</td>
				<td align="right">
				  <select name="type">
				    <option value="zip">zip</option>
				  </select>
				</td>
			  </tr>
		      <tr>
			    <td> </td>
				<td align="right">
				  <input type="submit" value="'.$GLOBALS['LANG']->getLL('message.btncreate').'">
				  <input type="button" value="'.$GLOBALS['LANG']->getLL('message.btncancel').'" onClick="javascript:location=\''.quickplorer_div::make_link('list',$dir,NULL).'\';">
				</td>
			  </tr>
			 </form>
			 </table>
			 <br />';
			 
		return implode('',$content);
	}

	
	
	function zip_items($dir,$name) {
		
		$name .= (strtolower(pathinfo($name, PATHINFO_EXTENSION))!='zip') ? '.zip' : '';
	
		$selitems = GeneralUtility::_POST('selitems');
		if (is_array($selitems))$cnt=count($selitems); else $cnt=0;
	
		//$cnt=count($selitems);
		$abs_dir=quickplorer_div::get_abs_dir($dir);
		
		$zipfile=GeneralUtility::makeInstance(quickplorer_zipfile::class);
		for($i=0;$i<$cnt;++$i) {
			$selitem=stripslashes($selitems[$i]);
			if(!$zipfile->add($abs_dir,$selitem)) {
				quickplorer_div::showError($selitem.': Failed adding item.');
			}
		}
		if(!$zipfile->save(quickplorer_div::get_abs_item($dir,$name))) {
			quickplorer_div::showError($name.': Failed saving zipfile.');
		}
		
		header('Location: '.quickplorer_div::make_link('list',$dir,NULL));
	}
}

if (defined('TYPO3') && $TYPO3_CONF_VARS['BE']['XCLASS']['ext/quickplorer/Classes/Controller/quickplorer_archive.php'])	{
	include_once($TYPO3_CONF_VARS['BE']['XCLASS']['ext/quickplorer/Classes/Controller/quickplorer_archive.php']);
}

