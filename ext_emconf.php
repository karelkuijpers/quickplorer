<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "quickplorer".
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
	'title' => 'Quickplorer',
	'description' => 
	'A backend module that makes you capable of exploring the files and folders of your entire document root. Browse directories. View and edit ascii files. Create, copy, move, delete, archive files and directories. Download and upload files. Change permissions on files and folders.',
	'category' => 'module',
	'version' => '11.0.2',
    'dependencies' => '',
    'conflicts' => '',
    'priority' => '',
    'loadOrder' => '',
    'module' => 'Backend',
	'state' => 'stable',
	'uploadfolder' => 0,
    'modify_tables' => '',
    'clearcacheonload' => 0,
    'lockType' => '',
	'createDirs' => '',
	'author' => 'Karel Kuijpers',
	'author_email' => 'karelkuijpers@gmail.com',
	'author_company' => 'parousia',
    'doNotLoadInFE' => 1,
    'CGLcompliance' => '',
    'CGLcompliance_note' => '',
    'constraints' => [
        'depends' => [
			'typo3' => '9.5.0-11.5.99',
        ]
    ]
);

